# Action Item: CI/CD for the Frontend

In this Action Item we will deploy the Frontend application built in the previous week. If you haven't already, we recommend you complete that Action Item before starting this one.

**Make sure you set aside at least 2 hours of focused, uninterrupted work and give your best.**

In order to deploy the frontend application we will:

1. Get an account on `GitLab`
2. Get an account on `AWS`
3. Create a `GitLab` repository for the app
4. Change the origin of our repository to the `Gitlab` 
5. Create and `S3` bucket and deploy manually
6. Automatize the deployment by creating a pipeline in `Gitlab`

### App Setup:
Install dependencies:
```bash 
npm install
```
Run in dev mode:
```bash 
npm start
```
Run in production mode:
```bash 
npm run production
```

### Steps

## 1. Get an account on `GitLab`

Go to [Gitlab](https://gitlab.com/users/sign_up) and get an account. We recommend using your `github` when signing up.
![Gitlab-Sing-up](examples/gitlab_sign_up.png)


1.2 Add your credit card to validate you account(you will not be able to run pipelines if you do not do this).

1.3 Add your `.ssh` key to the `Gitlab` account. See the [step by step guide here](https://portal.aws.amazon.com/billing/signup#/start).

**The two steps above are critical for you to be able to push code and run pipelines on Gitlab.**

*If you already have a Gitlab account an the `ssh` key setup you can skip the two steps above.*

## 2. Create an account on `AWS`

Go to [AWS](https://portal.aws.amazon.com/billing/signup#/start) and create an account. 

## 3. Create a `GitLab` repository for the app

3.1 Login to `Gitlab` by clicking [here](https://gitlab.com/users/sign_in)
3.2 Go to [Projects] and click on *New Project*:
![New Project](examples/new_project_gitlab.png)

3.3 In the next step select *Blank Project*:
![Blank Project](examples/blank_project_gitlab.png)

3.4 Add a name and remove the *README* option:
![Setup the Project](examples/new_project_gitlab_setup.png)

3.5 Click *Create Project*, you should see this:
![New Project](examples/new_project_view_gitlab.png)

Congratulations! In the next step, we will change the *origin* of the current repository and host our code on *Gitlab* instead.

4. Change the origin of our repository to the `Gitlab` 



---------------------


1.1 Run the app in production mode:
```bash 
npm run production
```
*Note: The [serve](https://www.npmjs.com/package/serve) package was added to mock a production server.*

1.2 Run the a diagnosis in `Chrome` using Lighthouse and analyze the results:

![Lighthouse](examples/diagnosis_lighthouse.png)

##### This are our results:
![results](examples/results_begin.png)
examples

##### Check your results and try to come up with ways to improve them. Do the same on your work applications or side projects.


#### 2. Add `Sentry` as a monitoring and performance tool

    1. Go to [Sentry](https://sentry.io/welcome/) and get an account
    2. Follow the instructions and set it up for your app
    3. Check both the issue section and the web vitals section


#### 3. Implement several optimizations with `Webpack`

Following the diagnosis, we proceed now to improve our app.

3.1 Switch webpack to [production](https://webpack.js.org/guides/production/) mode:

```bash
    # Install webpack merge
    npm install --save-dev webpack-merge
    # Rename the main config to common:
    mv webpack.config.js webpack.common.js
```

Create a `webpack` dev config:

```bash
    # Rename the main config to common:
    touch webpack.dev.js 
    touch webpack.prod.js
```

In `webpack.dev.config` add:
```diff
const { merge } = require('webpack-merge');
const common = require('./webpack.common.js');

module.exports = merge(common, {
    mode: 'development',
    devtool: 'inline-source-map',
    devServer: {
        port: 3000,
        hot: true
    },
});
```

In `webpack.prod.config`:
```diff
const { merge } = require('webpack-merge');
const common = require('./webpack.common.js');

module.exports = merge(common, {
    mode: 'production',
});
```

In `webpack.common.config`:
```diff
module.exports = {
    ...
-   mode: "development",
plugins: [new HtmlWebpackPlugin({
    template: path.join(__dirname, "public", "index.html")
})],
-    devServer: {
-        port: 3000,
-        hot: true
-    },
```

In `package.json` add:
```diff
    "scripts": {
-     "start": "webpack serve",
+     "start": "webpack serve --open --config webpack.dev.js",
-     "build": "webpack"
+     "build": "webpack --config webpack.prod.js"
    },
```

In production `Webpack` will:
- [minify javascript code](https://webpack.js.org/guides/production/#minification)
- apply [tree-shanking](https://webpack.js.org/guides/production/#minification) and remove dead code(unused packages, etc ...)

**You will need an extra setup to minify CSS.[Check it here](https://webpack.js.org/plugins/mini-css-extract-plugin/#minimizing-for-production).**

3.2. Compress the static assets in production using the [CompressionWebpackPlugin](https://webpack.js.org/plugins/compression-webpack-plugin/#root):

```bash
     npm install compression-webpack-plugin --save-dev
```

In `webpack.prod.js`:

```diff
+const CompressionPlugin = require("compression-webpack-plugin");

module.exports = {
+  plugins: [new CompressionPlugin()],
};
```

Go the extra mile and setup Brotli as [a compression algorithm](https://webpack.js.org/plugins/compression-webpack-plugin/#using-brotli) to produce even smaller assets.

Run the build and check the build folder:
```bash
    npm run build
```
![build_example](examples/build_example.png)

**Your files are now compressed and optimized on every build**


3.3 Setup the [Bundle Analyzer ](https://www.npmjs.com/package/webpack-bundle-analyzer)

```bash
    npm install --save-dev webpack-bundle-analyzer
```

In `webpack.prod.js`:

```diff
+const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;

module.exports = {
  plugins: [
    ...,
+   new BundleAnalyzerPlugin({
+       analyzerMode: "json",
+       generateStatsFile: true
    })
  ]
}
```

And in `package.json`:
```diff
  "scripts": {
    "build": "webpack --config webpack.prod.js",
    "start": "webpack serve --open --config webpack.dev.js",
    "test": "jest --watch",
+   "analyze": "npm run build && webpack-bundle-analyzer build/stats.json --port 5000",
    "production": "npm run build && serve build"
  },
```
You should see something like this:
![build_example](examples/bundle_analyzer.gif)


3.4 Setup Code Splitting by routes:

Check the [docs here](https://reactrouter.com/web/guides/code-splitting) or follow the instructions.

```bash
    npm install --save-dev @babel/plugin-syntax-dynamic-import
    npm install @loadable/component
    npm i --save-dev @types/loadable__component
```

In `App.tsx`:

```diff
import MainPage from './components/MainPage'
-import MoviePage from './components/MoviePage'
+import loadable from "@loadable/component";

+const Loading = () => (<h1>Loading</h1>)

+const LoadableMoviePage = loadable(() => import("./components/MoviePage"), {
+    fallback: <Loading />
+});

export default function App() {
    return (
        <div>
            <Router>
                <Switch>
                    <Route path="/movie/:id">
-                       <MoviePage></MoviePage>
+                       <LoadableMoviePage></LoadableMoviePage>
                    </Route>
                    <Route path="/*">
                        <MainPage></MainPage>
                    </Route>
                </Switch>
            </Router>

        </div>
    )
}
```

Now run the bundle analysis again:
```bash
    npm run analyze
```
The Movie Page component will be now compiled to a separated file and lazy loading only when needed.
![examples/lazy_loaded_bundles.png](examples/lazy_loaded_bundles.png)


## Check the results and iterate:

We got a score of 84 after a few optimization. That is more than 20% increase. Keep researching, apply optimizations and testing in an iterative cycle. 

    May the DOM be with you!

![result-optimization](examples/results_end.png)


### 4. Optimize our code in `React`

1. You can use the React API directly to lazy load components with `React.lazy()`. No need for an additional library(we used loadable).

2. Apply `useCallback` and `React.memo` to avoid re-renders of your components:

Example in `Pagination.tsx`:

```diff
-import React from 'react'
+import React, {useCallback} from 'react'

// Memoization of a functional component
const PaginationLimitButton = styled.button`
    box-sizing: content-box;
    font-weight: 100;
    :hover{
        background: #227093;
        cursor: pointer;
    }
`  

+ const PaginationLimitButtonMemoized = React.memo(PaginationLimitButton)

function Pagination({pageTotal, currentPage, setCurrentPage}:PaginationProps) {
+    const goBack = useCallback(
+        () => {
+            setCurrentPage(currentPage-1)
+        },
+        [ pageTotal, setCurrentPage],
+    )
    return (
        <PaginationContainer>
            <PaginationLimitButton data-testid="btn-first" onClick={() => (setCurrentPage(1))} disabled={currentPage == 1}>First</PaginationLimitButton>
+          <PaginationLimitButtonMemoized data-testid="btn-next" onClick={goBack} disabled={currentPage == 1}>Previous</PaginationLimitButtonMemoized>
-          <PaginationLimitButton data-testid="btn-next" onClick={() => setCurrentPage(currentPage-1)} disabled={currentPage == 1}>Previous</PaginationLimitButton>
            <p>Page {currentPage} of {pageTotal}</p>
            <PaginationLimitButton data-testid="btn-prev" onClick={() => (setCurrentPage(currentPage+1))} disabled={currentPage == pageTotal}>Next</PaginationLimitButton>
            <PaginationLimitButton data-testid="btn-last" onClick={() => (setCurrentPage(pageTotal))}  disabled={currentPage == pageTotal}>Last</PaginationLimitButton>
        </PaginationContainer>
    )
}
```

Use the `react-dev-tools` to check that adding `useCallback` and `useMemo` reduces the number of re-renders following [the guide here](https://brycedooley.com/debug-react-rerenders/). 

---
## Wrapping up

1. Keep adding optimization from webpack
2. Look into: CSS Minification, Lazy Loading Images, etc
3. Analyze -> Diagnose-> Improve



### Made with :orange_heart: in Berlin by @CodeWithDragos
